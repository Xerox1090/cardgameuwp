﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    public class CardDeck
    {
        //declare the list of cards in the deck
        private List<Card> _cardList;

        public CardDeck()
        {
            _cardList = new List<Card>();
        }
        public int CardCount
        {
            get { return _cardList.Count; }
        }
    }
}
