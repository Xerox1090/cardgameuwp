﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    public enum Cardsuit
    {
    DIAMONDS = 1,
    CLUBS = 2,
    HEARTS = 3,
    SPADES = 4
    }
    /// <summary>
    /// represents a card game playing card 
    /// </summary>
    public class Card
    {
        /// <summary>
        /// the value of the cared, 1 -> 13
        /// </summary>
        private byte _value;
        
        /// <summary>
        /// the suit of the card 
        /// </summary>
        private Cardsuit _suit;
        public Card(byte value, Cardsuit suit)
        {
            _value = value;
            _suit = suit;

        }
        public byte Value
        {
            get { return _value; }
            set { _value = value; }
        }
         //ToDo declare a read/write property _suit
         public Cardsuit Suit
        {
            get { return _suit; }
            set { _suit = value; }
        }
        public string CardName
        {
            get { return "TODO"; }
        }
    }
}
