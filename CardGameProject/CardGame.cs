﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    public struct GameScore
    {
        //declare the variables that represent the score
        private int _playerScore;

        private int _houseScore;
        public GameScore(int playerScore, int houseScore)
        {
            _playerScore = playerScore;
            _houseScore = houseScore;
        }
        public int PlayerScore
        {
            get { return _playerScore; }
            set { _playerScore = value; }
        }
        public int HouseScore
        {
            get { return _houseScore; }
            set { _houseScore = value; }
        }
    }
    public class CardGame
    {
        //define the card deck
        private CardDeck _cardDeck;

        private GameScore _score;

        //declare the current card played by the house
        private Card _houseCard;

        //declare the current card player by the player
        private Card _playerCard;

        public CardGame()
        {
            _cardDeck = new CardDeck();
            _score = new GameScore(0, 0);
            _houseCard = null;
            _playerCard = null;

        }
        public void Play()
        {

        }
        private byte DetermineCardRank(Card card)
        {
            return 0;
        }
        public GameScore Score
                {
                    get { return _score; }
                }
        public Card PlayerCard
        {
            get { return _playerCard; }
        }
        public Card HouseCard
        {
            get { return _houseCard; }
        }
        public bool PlayerWins
        {
            get { return _score.PlayerScore > _score.HouseScore; }
        }
        public bool HouseWins
        {
            get { return _score.HouseScore > _score.PlayerScore; }
        }

        public bool IsOver
        {
            get { return _cardDeck.CardCount == 0; }
        }
        
    }
}
